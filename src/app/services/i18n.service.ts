import { Injectable } from '@angular/core';
import { TranslateService } from '@ngx-translate/core/ngx-translate-core';

export enum LANGUAGE {
  EN = 'en',
  FR = 'fr',
}

@Injectable({
  providedIn: 'root'
})
export class I18nService {

  public currentLanguage: string = LANGUAGE.FR;

  constructor(private translate: TranslateService) { }

  /**
   * Initialisation de la langue utilisée dans l"application
   */
  public initLanguage(): void {
    this.translate.setDefaultLang(this.currentLanguage);
    this.translate.use(this.currentLanguage);
  }

  public switchLanguage(): void {
    this.currentLanguage = this.currentLanguage === LANGUAGE.FR ? LANGUAGE.EN : LANGUAGE.FR;
    this.initLanguage();
  }

  public instantTranslation(key: string): string {
    return this.translate.instant(key);
  }
}
