import { Pipe, PipeTransform } from '@angular/core';
import { DEVISE } from '../models/devise';
import { I18nService, LANGUAGE } from '../services/i18n.service';

@Pipe({name: 'convertCurrency'})
export class CurrencyConversion implements PipeTransform {

  usdToEurChange: number = 0.92;
  eurtoUsdChange: number = 1.09;

  constructor(private i18n: I18nService) {}

  transform(ammount: any, devise: DEVISE): string {
    if (devise === DEVISE.Euros) {
      if (this.i18n.currentLanguage === LANGUAGE.EN) {
        ammount = ammount * this.eurtoUsdChange;
        ammount = ammount.toFixed(2);
        return ammount + ' ' + '$';
      }
      return ammount + ' ' + '€';
    } else {
      if (this.i18n.currentLanguage === LANGUAGE.FR) {
        ammount = ammount * this.usdToEurChange;
        ammount = ammount.toFixed(2);
        return ammount + ' ' + '€';
      }
      return ammount + ' ' + '$';
    }
  }
}
